<?php

function getDb()
{
    return new PDO("mysql:host=localhost;dbname=gsb;charset=utf8", "root", "root",
        array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
        
}

 function prepareChaineHtml($schaine)
 {    
     $text = htmlspecialchars($schaine);
     return $text;
 }
 
 function moisEnFrancais($schaine)
 {
     $lesMois = array(
         
         'January'   => 'Janvier',
         'February'  => 'Février',
         'March'     => 'Mars',
         'April'     => 'Avril',
         'May'       => 'Mai',
         'June'      => 'Juin',
         'July'      => 'Juillet',
         'August'    => 'Août',
         'September' => 'Septembre',
         'October'   => 'Octobre',
         'November'  => 'Novembre',
         'December'  => 'Décembre',      
     );
     
     return $lesMois[$schaine];
 }
 
 function getTabMoisDeAnnee()
 {
     $tabMois = array(
         
         '0'  => 'Janvier',
         '1'  => 'Février',
         '2'  => 'Mars',
         '3'  => 'Avril',
         '4'  => 'Mai',
         '5'  => 'Juin',
         '6'  => 'Juillet',
         '7'  => 'Août',
         '8'  => 'Septembre',
         '9'  => 'Octobre',
         '10' => 'Novembre',
         '11' => 'Décembre',      
     );
     
     return $tabMois;
 }
 
function getTabMoisDeAnnee_0()
 {
     $tabMois = array(
         
         '01'  => 'Janvier',
         '02'  => 'Février',
         '03'  => 'Mars',
         '04'  => 'Avril',
         '05'  => 'Mai',
         '06'  => 'Juin',
         '07'  => 'Juillet',
         '08'  => 'Août',
         '09'  => 'Septembre',
         '10'  => 'Octobre',
         '11' => 'Novembre',
         '12' => 'Décembre',      
     );
     
     return $tabMois;
 }
 
     
 function getIdVisiteur(){
    try{
        $pdo = getDb();
    }

    catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage() . "<br/>";
        die();

    }
    $stmt = $pdo->prepare('SELECT id FROM visiteur WHERE login= :log ');
    $stmt->bindValue(':log', $_SESSION['connect'], PDO::PARAM_STR);
    $stmt->execute();

    $idVisiteur = $stmt->fetch();

    return $idVisiteur[0];

 } 

function bindLigneFraisForfait(string $idVisiteur, string $mois, int $quantite, string $idFrais){
    try{
      $pdo = getDb();
    }

    catch (PDOException $e) {
      print "Erreur !: " . $e->getMessage() . "<br/>";
      die();            
    }

    $stmt = $pdo->prepare('INSERT INTO ligneFraisForfait (idVisiteur, mois,idFraisForfait, quantite) VALUES (:idVisiteur, :mois, :idFraisForfait, :quantite');
    $stmt->bindParam(':idVisiteur',$idVisiteur, PDO::PARAM_STR);
    $stmt->bindParam(':mois',$mois, PDO::PARAM_STR);            
    $stmt->bindParam(':quantite',$quantite, PDO::PARAM_INT); 
    $stmt->bindParam(':idFraisForfait',$idFraisForfait, PDO::PARAM_STR);

    $stmt->execute();

    $pdo = null;
}



?>

